import org.junit.Test;

import static org.junit.Assert.*;

public class PokerHandTests {

	@Test
	public void canCheckRoyalFlush() {
		assertEquals(new PokerHand("AS KS QS JS TS").getHandRank(), PokerHandCategory.ROYAL_FLUSH);
		assertEquals(new PokerHand("AD KD QD TD JD").getHandRank(), PokerHandCategory.ROYAL_FLUSH);
		assertEquals(new PokerHand("QH KH AH JH TH").getHandRank(), PokerHandCategory.ROYAL_FLUSH);
		assertEquals(new PokerHand("AC JC QC KC TC").getHandRank(), PokerHandCategory.ROYAL_FLUSH);
		assertNotEquals(new PokerHand("AS 2S 3S 4S 6S").getHandRank(), PokerHandCategory.ROYAL_FLUSH);
	}

	@Test
	public void canCheckStrightFlush() {
		assertEquals(new PokerHand("KS QS JS TS 9S").getHandRank(), PokerHandCategory.STRAIGHT_FLUSH);
        assertEquals(new PokerHand("5D 4D 3D 2D AD").getHandRank(), PokerHandCategory.STRAIGHT_FLUSH);
		assertNotEquals(new PokerHand("6D 4D 3D 2D AD").getHandRank(), PokerHandCategory.STRAIGHT_FLUSH);
	}

	@Test
	public void canCheckFourOfAKind() {
		assertEquals(new PokerHand("AS AH AD AC JD").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
		assertEquals(new PokerHand("AS AH AD JD AC").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
		assertEquals(new PokerHand("AS AH JD AD AC").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
		assertEquals(new PokerHand("AS JD AH AD AC").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
		assertEquals(new PokerHand("JD AS AH AD AC").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
		assertEquals(new PokerHand("3S 3H 3D 3C 5D").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);

		assertNotEquals(new PokerHand("JD JS AH AD AC").getHandRank(), PokerHandCategory.FOUR_OF_A_KIND);
	}

	@Test
	public void canCheckFullHouse() {
        assertEquals(new PokerHand("TS TH KD KC KH").getHandRank(), PokerHandCategory.FULL_HOUSE);
		assertEquals(new PokerHand("TS KD TH KC KH").getHandRank(), PokerHandCategory.FULL_HOUSE);
		assertEquals(new PokerHand("TS KD KC TH KH").getHandRank(), PokerHandCategory.FULL_HOUSE);
		assertEquals(new PokerHand("TS KD KC KH TH").getHandRank(), PokerHandCategory.FULL_HOUSE);
		assertEquals(new PokerHand("KD KC KH TS TH").getHandRank(), PokerHandCategory.FULL_HOUSE);
	}

    @Test
    public void canCheckFlush() {
		assertEquals(new PokerHand("AS 2S 3S 4S 6S").getHandRank(), PokerHandCategory.FLUSH);
		assertEquals(new PokerHand("AH 2H 3H 4H 6H").getHandRank(), PokerHandCategory.FLUSH);
		assertNotEquals(new PokerHand("3S 4S 8H AS KS").getHandRank(), PokerHandCategory.FLUSH);
	}

	@Test
	public void canCheckStraight() {
		assertEquals(new PokerHand("AS KD QH JC TH").getHandRank(), PokerHandCategory.STRAIGHT);
		assertEquals(new PokerHand("7S 6D 4C 5H 3H").getHandRank(), PokerHandCategory.STRAIGHT);
		assertEquals(new PokerHand("AS 2H 3D 4C 5H").getHandRank(), PokerHandCategory.STRAIGHT);
	}

	@Test
	public void canCheckThreeOfAKind() {
		assertEquals(new PokerHand("2S 2H 2D 4C 5H").getHandRank(), PokerHandCategory.THREE_OF_A_KIND);
		assertEquals(new PokerHand("AS AH 4C AD 5H").getHandRank(), PokerHandCategory.THREE_OF_A_KIND);
		assertEquals(new PokerHand("AS 4C AH AD 5H").getHandRank(), PokerHandCategory.THREE_OF_A_KIND);
	}

	@Test
	public void canCheckTwoPairs() {
		assertEquals(new PokerHand("JD AS AH 4C 4H").getHandRank(), PokerHandCategory.TWO_PAIRS);
		assertEquals(new PokerHand("AS JD AH 4C 4H").getHandRank(), PokerHandCategory.TWO_PAIRS);
		assertEquals(new PokerHand("AS AH JD 4C 4H").getHandRank(), PokerHandCategory.TWO_PAIRS);
	}

	@Test
	public void canCheckOnePair() {
		assertEquals(new PokerHand("JD AS KH 4C 4H").getHandRank(), PokerHandCategory.ONE_PAIR);
		assertEquals(new PokerHand("4D AS KH JC 4H").getHandRank(), PokerHandCategory.ONE_PAIR);
	}

	@Test
	public void canCheckHighCard() {
		assertEquals(new PokerHand("JD AS KH QC 4H").getHandRank(), PokerHandCategory.HIGH_CARD);
		assertEquals(new PokerHand("4D AS KH JC QH").getHandRank(), PokerHandCategory.HIGH_CARD);
	}

	@Test
	public void canPassPredefinedTests() {
		assertEquals(new PokerHand("9C TC JC QC KC").compareWith(new PokerHand("9C 9H 5C 5H AC")), Result.WIN);
		assertEquals(new PokerHand("TS JS QS KS AS").compareWith(new PokerHand("AC AH AS AS KS")), Result.WIN);
		assertEquals(new PokerHand("TS JS QS KS AS").compareWith(new PokerHand("TC JS QC KS AC")), Result.WIN);
		assertEquals(new PokerHand("TS JS QS KS AS").compareWith(new PokerHand("QH QS QC AS 8H")), Result.WIN);
		assertEquals(new PokerHand("AC AH AS AS KS").compareWith(new PokerHand("TC JS QC KS AC")), Result.WIN);
		assertEquals(new PokerHand("AC AH AS AS KS").compareWith(new PokerHand("QH QS QC AS 8H")), Result.WIN);
		assertEquals(new PokerHand("7H 8H 9H TH JH").compareWith(new PokerHand("JH JC JS JD TH")), Result.WIN);
		assertEquals(new PokerHand("7H 8H 9H TH JH").compareWith(new PokerHand("4H 5H 9H TH JH")), Result.WIN);
		assertEquals(new PokerHand("7H 8H 9H TH JH").compareWith(new PokerHand("7C 8S 9H TH JH")), Result.WIN);
		assertEquals(new PokerHand("7H 8H 9H TH JH").compareWith(new PokerHand("TS TH TD JH JD")), Result.WIN);
		assertEquals(new PokerHand("7H 8H 9H TH JH").compareWith(new PokerHand("JH JD TH TC 4C")), Result.WIN);
		assertEquals(new PokerHand("JH JC JS JD TH").compareWith(new PokerHand("4H 5H 9H TH JH")), Result.WIN);
		assertEquals(new PokerHand("JH JC JS JD TH").compareWith(new PokerHand("7C 8S 9H TH JH")), Result.WIN);
		assertEquals(new PokerHand("JH JC JS JD TH").compareWith(new PokerHand("TS TH TD JH JD")), Result.WIN);
		assertEquals(new PokerHand("JH JC JS JD TH").compareWith(new PokerHand("JH JD TH TC 4C")), Result.WIN);
		assertEquals(new PokerHand("4H 5H 9H TH JH").compareWith(new PokerHand("TS TH TD JH JD")), Result.LOSS);
		assertEquals(new PokerHand("7C 8S 9H TH JH").compareWith(new PokerHand("TS TH TD JH JD")), Result.LOSS);
		assertEquals(new PokerHand("7C 8S 9H TH JH").compareWith(new PokerHand("JH JD TH TC 4C")), Result.WIN);
		assertEquals(new PokerHand("TS TH TD JH JD").compareWith(new PokerHand("JH JD TH TC 4C")), Result.WIN);
		assertEquals(new PokerHand("2S 3H 4H 5H 6D").compareWith(new PokerHand("2S 3H 4D 5H 6C")), Result.DRAW);
		assertEquals(new PokerHand("TH JH QH KH AH").compareWith(new PokerHand("TC JC QC KC AC")), Result.DRAW);
		assertEquals(new PokerHand("2H 3C 3D 3S 6H").compareWith(new PokerHand("2C 3D 4D 5C 6C")), Result.LOSS);
	}


	@Test
	public void canPassTieBreakers() {
		assertEquals(new PokerHand("TC TH 5C 5H KH").compareWith(new PokerHand("9C 9H 5C 5H AC")), Result.WIN);
		assertEquals(new PokerHand("TS TD KC JC 7C").compareWith(new PokerHand("JS JC AS KC TD")), Result.LOSS);
		assertEquals(new PokerHand("7H 7C QC JS TS").compareWith(new PokerHand("7D 7C JS TS 6D")), Result.WIN);
		assertEquals(new PokerHand("5S 5D 8C 7S 6H").compareWith(new PokerHand("7D 7S 5S 5D JS")), Result.LOSS);
		assertEquals(new PokerHand("AS AD KD 7C 3D").compareWith(new PokerHand("AD AH KD 7C 4S")), Result.LOSS);
		assertEquals(new PokerHand("TC JS QC KS AC").compareWith(new PokerHand("QH QS QC AS 8H")), Result.WIN);
		assertEquals(new PokerHand("4H 5H 9H TH JH").compareWith(new PokerHand("7C 8S 9H TH JH")), Result.WIN);
		assertEquals(new PokerHand("4H 5H 9H TH JH").compareWith(new PokerHand("JH JD TH TC 4C")), Result.WIN);
		assertEquals(new PokerHand("2S 3H 4D 5H 6D").compareWith(new PokerHand("5H 6D 7H 8C 9C")), Result.LOSS);
		assertEquals(new PokerHand("2H 3H 4H 5H 7H").compareWith(new PokerHand("2D 3D 4D 5D 8D")), Result.LOSS);
		assertEquals(new PokerHand("2S 2H 2D 5H 6D").compareWith(new PokerHand("5H 5D 5H 8C 9C")), Result.LOSS);
		assertEquals(new PokerHand("2H 3H 4H 5H 6H").compareWith(new PokerHand("5H 6H 7H 8H 9H")), Result.LOSS);
		assertEquals(new PokerHand("TH TH TH TH AS").compareWith(new PokerHand("9C 9C 9C 9C 2S")), Result.WIN);
		assertEquals(new PokerHand("TH TH TH AH AS").compareWith(new PokerHand("9C 9C 9C 2C 2S")), Result.WIN);
		assertEquals(new PokerHand("2H 4H 6H 8H AS").compareWith(new PokerHand("3C 5C 6C 8C JS")), Result.WIN);
		assertEquals(new PokerHand("2H 2H 2H AH AS").compareWith(new PokerHand("2C 2C 2C JC JS")), Result.WIN);
		assertEquals(new PokerHand("2H 2H 5H AH AS").compareWith(new PokerHand("2C 2C 6C AC AS")), Result.LOSS);
		assertEquals(new PokerHand("AS 2S 5S 8S QS").compareWith(new PokerHand("KS JS 5S 8S QS")), Result.WIN);
	}

}
