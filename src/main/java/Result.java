public enum Result {
	WIN,
	LOSS,
	DRAW;

	public static Result compareHandsByCategory(PokerHandCategory a, PokerHandCategory b) {
		return Result.compareHandsByValue(a.getValue(), b.getValue());
	}

	public static Result compareHandsByValue(Integer a, Integer b) {
		if (a > b) {
			return Result.WIN;
		} else if (a < b) {
			return Result.LOSS;
		}
		return Result.DRAW;
	}
}
