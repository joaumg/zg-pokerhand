public enum CardShape {
    S,
    H,
    D,
    C;

    public static CardShape get(char ch) {
        switch (ch) {
            case 'S': return CardShape.S;
            case 'H': return CardShape.H;
            case 'D': return CardShape.D;
            case 'C': return CardShape.C;
            default:
                System.out.println("Unknown card shape: " + ch);
                return null; // @TODO: handle case properly instead of risking a NPE
        }
    }
}
