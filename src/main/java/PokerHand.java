import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PokerHand {
	private List<Card> cards;
	private PokerHandCategory handRank;

	public PokerHand(String input) {
		this.cards = new ArrayList<>();
		for (String card : input.split(" ")) {
			cards.add(new Card(card));
		}
		Collections.sort(cards, Comparator.comparing(Card::getValue));
		handRank = PokerHandCategory.classifyHand(getCards());
	}

	public Result compareWith(PokerHand anotherHand) {
	    Result result = Result.compareHandsByCategory(handRank, anotherHand.getHandRank());
	    if (Result.DRAW.equals(result)) { // Tie break
			return tieBreak(anotherHand);
		}
		return result;
	}

	public PokerHandCategory getHandRank() {
		return handRank;
	}

	public List<Card> getCards() {
		return cards;
	}

	private List<Integer> orderCardByImportance(List<Card> cards) {
	    // https://www.mkyong.com/java8/java-8-collectors-groupingby-and-mapping-example/
	    // order by frequency and value
        if (getHandRank().equals(PokerHandCategory.HIGH_CARD) || getHandRank().equals(PokerHandCategory.FLUSH)) {
        	return cards.stream().map(Card::getValue).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		}
	    List<Integer> cardValues = cards.stream().map(Card::getValue)
				.collect(Collectors.toList());
		Map<Integer, Long> result = cardValues.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		Map<Integer, Long> finalMap = new LinkedHashMap<>();
		result.entrySet().stream()
				.sorted(Map.Entry.<Integer, Long>comparingByValue().reversed())
				.forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));
		List<Integer> orderedByFrequencyAndValue = new ArrayList<>();
		finalMap.forEach((value, freq) -> {
			for (int i = 0; i < freq; i++) {
				orderedByFrequencyAndValue.add(value);
			}
		});
		return orderedByFrequencyAndValue;
	}

	private Result tieBreak(PokerHand rivalHand) {
		List<Integer> groupedCards = orderCardByImportance(getCards());
		List<Integer> rivalGroupedCards = orderCardByImportance(rivalHand.getCards());

		for (int i = 0; i < 5; ++i) {
			Result result = Result.compareHandsByValue(groupedCards.get(i), rivalGroupedCards.get(i));
			if (Result.DRAW.equals(result)) {
				continue;
			}
			return result;
		}

		return Result.DRAW;
	}
}
