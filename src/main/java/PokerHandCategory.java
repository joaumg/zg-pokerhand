import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum PokerHandCategory {
	HIGH_CARD(0),
	ONE_PAIR(1),
	TWO_PAIRS(2),
	THREE_OF_A_KIND(3),
	STRAIGHT(4),
	FLUSH(5),
	FULL_HOUSE(6),
	FOUR_OF_A_KIND(7),
	STRAIGHT_FLUSH(8),
	ROYAL_FLUSH(9);

	private int value;

	PokerHandCategory(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public static PokerHandCategory classifyHand(List<Card> cards) {
		if (isRoyalFlush(cards)) {
			return PokerHandCategory.ROYAL_FLUSH;
		}
		if (isStrightFlush(cards)) {
			return PokerHandCategory.STRAIGHT_FLUSH;
		}
		if (isFourOfAKind(cards)) {
			return PokerHandCategory.FOUR_OF_A_KIND;
		}
		if (isFullHouse(cards)) {
			return PokerHandCategory.FULL_HOUSE;
		}
		if (isFlush(cards)) {
			return PokerHandCategory.FLUSH;
		}
		if (isStraight(cards)) {
			return PokerHandCategory.STRAIGHT;
		}
		if (isThreeOfAKind(cards)) {
			return PokerHandCategory.THREE_OF_A_KIND;
		}
		if (isTwoPairs(cards)) {
			return PokerHandCategory.TWO_PAIRS;
		}
		if (isOnePair(cards)) {
			return PokerHandCategory.ONE_PAIR;
		}
		return PokerHandCategory.HIGH_CARD;
	}

	public static boolean isOnePair(List<Card> cards) {
		for (int i = 0; i < 4; ++i) {
			if (cards.get(i).getValue() == cards.get(i + 1).getValue()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isTwoPairs(List<Card> cards) {
		// 2/2/1
		if (cards.get(0).getValue() == cards.get(1).getValue() && cards.get(2).getValue() == cards.get(3).getValue()) {
			return true;
		}
		// 2/1/2
		if (cards.get(0).getValue() == cards.get(1).getValue() && cards.get(3).getValue() == cards.get(4).getValue()) {
			return true;
		}
		// 1/2/2
		if (cards.get(1).getValue() == cards.get(2).getValue() && cards.get(3).getValue() == cards.get(4).getValue()) {
			return true;
		}
		return false;
	}

	public static boolean isThreeOfAKind(List<Card> cards) {
		// first three
		if (cards.get(0).getValue() == cards.get(1).getValue() && cards.get(0).getValue() == cards.get(2).getValue()) {
			return true;
		}
		// middle three
		if (cards.get(1).getValue() == cards.get(2).getValue() && cards.get(1).getValue() == cards.get(3).getValue()) {
			return true;
		}
		// last three
		if (cards.get(2).getValue() == cards.get(3).getValue() && cards.get(2).getValue() == cards.get(4).getValue()) {
			return true;
		}
		return false;
	}

	public static boolean isStraight(List<Card> cards) {
	    // handle the cases where A is 1 instead of 14
		if (cards.stream().map(Card::getValue).collect(Collectors.toList()).containsAll(Arrays.asList(2, 3, 4, 5, 14))) {
			return true;
		}
		for (int i = 1; i < 5; i++) {
			if (cards.get(0).getValue() + i != cards.get(i).getValue()) {
				return false;
			}
		}
		return true;
	}

	public static boolean isFlush(List<Card> cards) {
	    return sameShade(cards);
	}

	/**
	 * Converts all cards to their numeric value and check if there is three and two of same
	 * @param cards
	 * @return
	 */
	public static boolean isFullHouse(List<Card> cards) {
		// first two and last two are equal
		if (cards.get(0).getValue() != cards.get(1).getValue() || cards.get(3).getValue() != cards.get(4).getValue()) {
			return false;
		}
		// middle is equal to left or right
		if (cards.get(2).getValue() == cards.get(1).getValue() || cards.get(2).getValue() == cards.get(3).getValue()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if first four or last four (numerically ordered cards) are equal
	 * @param cards
	 * @return
	 */
	public static boolean isFourOfAKind(List<Card> cards) {
		boolean firstFour = true;
		for (int i = 1; i < 4; ++i) {
			if (cards.get(0).getValue() != cards.get(i).getValue()) {
				firstFour = false;
			}
		}
		if (firstFour) {
			return true;
		}
		// last four
		for (int i = 2; i < 5; ++i) {
			if (cards.get(1).getValue() != cards.get(i).getValue()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks for a Straight Flush, short-circuits looking the shades before
	 * @param cards
	 * @return
	 */
	public static boolean isStrightFlush(List<Card> cards) {
	    return sameShade(cards) && isStraight(cards);
	}

	/**
	 * Checks for a royal flush (only four available combinations)
	 * @param cards
	 * @return
	 */
	public static boolean isRoyalFlush(List<Card> cards) {
		if (!sameShade(cards)) {
			return false;
		}
        return cards.stream().map(Card::getValue).collect(Collectors.toList())
				.containsAll(Arrays.asList(10, 11, 12, 13, 14)); // @TODO: find a better way to represent this as [T, J, Q, K, A]
	}

	/**
	 * Removes the card values and spaces and compares all chars to equal the first char of the sequence
	 * @param cards
	 * @return the result of the comparison
	 */
	public static boolean sameShade(List<Card> cards) {
        CardShape shape = cards.get(0).getShape();
        return cards.stream().allMatch((card) -> card.getShape().equals(shape));
	}
}
