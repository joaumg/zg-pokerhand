public class Card {
    private CardShape shape;
    private Integer value;

    Card(String card) {
        this.value = getCardValue(card.charAt(0));
        this.shape = CardShape.get(card.charAt(1));
    }

    public CardShape getShape() {
        return shape;
    }

    public Integer getValue() {
        return value;
    }

    private Integer getCardValue(char ch) {
        switch (ch) {
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'T': return 10;
            case 'J': return 11;
            case 'Q': return 12;
            case 'K': return 13;
            case 'A': return 14;
            default:
                System.out.println("Unknown card value: " + ch);
                return -1;
        }
    }

}
